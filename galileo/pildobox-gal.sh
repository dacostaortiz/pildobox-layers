#!/bin/bash
##### PILDOBOX GALILEO START #####

cd $HOME
git clone --branch daisy git://git.yoctoproject.org/poky iotdk
cd iotdk
git clone git://git.yoctoproject.org/meta-intel-quark
git clone --branch daisy git://git.yoctoproject.org/meta-intel-iot-middleware
git clone --branch daisy git://git.yoctoproject.org/meta-intel-galileo
git clone --branch dizzywic git://git.yoctoproject.org/meta-intel-iot-devkit
git clone --branch daisy http://github.com/openembedded/meta-openembedded.git meta-oe
git clone git://git.yoctoproject.org/meta-oracle-java
git clone https://dacostaortiz@bitbucket.org/dacostaortiz/pildobox-layers.git
source oe-init-build-env build
# still in $HOME/iotdk
cd $HOME/iotdk/build/conf/
cp $HOME/iotdk/pildobox-layers/galileo/patches/bblayers.patch .
patch bblayers.conf < bblayers.patch
rm bblayers.patch
cp $HOME/iotdk/pildobox-layers/galileo/patches/auto.conf .
cd $HOME/iotdk/build
bitbake iot-devkit-prof-dev-image
